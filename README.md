# README #

Open CV Template Project for Visual Studio 2013

### What is this repository for? ###

* Maintain a Template project openCV
* AIM: To use a visual studio wizard to create projects from this template

### How do I get set up? ###

* Download and Extract OpenCV 2.4.9.exe
* Add Environment variable %OPENCV_DIR% => "Extracted Directory\opencv\build\x??\vc12"
* Configure PATH to include %OPENCV_DIR%\bin